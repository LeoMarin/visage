workspace "Visage"
    architecture "x64"
    startproject "Visage"
    configurations
	{
		"Debug",
		"Release"
	}
	outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

	project "Visage"
		location "Visage"
		kind "ConsoleApp"
		language "C++"
		cppdialect "C++17"
		staticruntime "on"

		targetdir 	("bin/" 	.. outputdir .. "/%{prj.name}")
		objdir 		("bin/int/" .. outputdir .. "/%{prj.name}")

		files 
		{
			"%{prj.name}/**.h",
			"%{prj.name}/**.cpp",
			
			"%{prj.name}/*/**.h",
			"%{prj.name}/*/**.cpp"
		}
		
		includedirs
		{
			"%{prj.name}/includes"
		}

		filter "system:windows"
			systemversion "latest"
		
		filter "configurations:Debug"
			defines "VT_DEBUG"
			runtime "Debug"
			symbols "on"

		filter "configurations:Release"
			defines "VT_RELEASE"
			runtime "Release"
			optimize "on"