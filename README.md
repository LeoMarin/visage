# Visage Technologies Test Project

Run "Win-Premake.bat" to generate VisualStudio 2019 solution files

## Image Class

Image class contains all requested functionality for this procjet.\
Project contains 3 main parts:
+ Parsing JSON data
+ Calculating pixels that are contained inside given polygons
+ Save calculated data in image file

Image class data:
+ regions : vector of regions containing all coordinate values for all positions
+ fb : heap initialized array containing color values for all pixels
+ x, y : intagers containing image size

## ParseJson

ParseJson method reads JSON data given in testCase files. It generates vector of Regions.\
Each Region contains vector of Positions defined by x and y coordinates.\
ParseJson also initializes FrameBuffer array, which will contain color data for all pixels in the image

## CalculatePixelColors

CalculatePixelColor loops over every pixel in the image and calculates color value.\
CalculatePixelColor uses Axis Aligned Bounding Boxes (AABB) for optimization. AABB works by finding a bounding box for every 
region and only pixel that fall inside that bounding box are tested if they fall inside of the region.

To check if pixel is inside region we use "Triangle Test" defined by Didier Badouel in "Graphic Gems, 1990". The test 
consists of creating all posible triangles (from the list of region vertices) that have 1 same vertex. After that we 
test if point is inside every of the triangles. If the point is inside odd number of triangles it is inside the region 
and if point is in even number of triangles it is outside of the region.

CalculatePixelColor uses 3 helper functions:
+ bool PixelIsInRegion(const std::vector<Position>& region, const Position& pixelPosition)
+ bool PixelIsInTriangle(const Position& A, const Position& B, const Position& C, const Position& P)
+ float CrossProductZComponent(const Position& A, const Position& B, const Position& P)

#### PixelIsInRegion

PixelIsInRegion method takes vector of positions that define the current region and position of current pixel as input. 
This method checks if current pixel is inside current region. Region is devided into triangles and number of triangles 
that pixel is inside is calculated. PixelIsInRegion returns true if point is in region otherwise it returns false.

#### PixelIsInTriangle

PixelIsInTriangle method takes position of triangle vertices (A, B and C) and position of current pixel (P) as input. 
P is inside ABC if and only if vectors ABxAP, BCxBP and CAxCP point in the same direction relative to triangle plane. 
This means z component of all 3 cross products must be all positive or all negative. It returns true if point is in
triangle otherwise false.

#### CrossProductZComponent

CrossProductZComponent method takes position of 3 points (A, B and P) and calculates Z component of Cross product of vectors AB and AP.

## DrawImage

DrawImage method writes out FrameBuffer in ppm format. Values in FrameBuffer are 0 or 1 and have to be scaled to 0-255 range.\
Image is converted from ppm format to png so it can be displayed here.

![generated image](Visage/images/out.png)

## TestCases

TestCases 1 and 2 are given in the task. TestCases 3-6 test edge cases.

![TestCase3](Visage/images/testCase3.png)

![TestCase4](Visage/images/testCase4.png)

![TestCase5](Visage/images/testCase5.png)

![TestCase6](Visage/images/testCase6.png)