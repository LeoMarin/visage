#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stack>
#include <math.h>

#include "image.h"

Image::Image(const char* fileName)
{
	// Get region and position data from JSON file
	ParseJson(fileName);
	
	// Calculate image values
	CalculatePixelColors();

	// Draw image
	DrawImage(fileName);
}

void Image::DrawImage(const char* fileName)
{	
	// Output FB as Image im ppm format
	std::cout << "Writing image to file..." << std::endl;

	// Get file path
	std::string filePath = "images/" + std::string(fileName) + ".ppm";

	// open .ppm file
	std::ofstream ofs;
	ofs.open(filePath);
	ofs << "P3\n" << sizeX << " " << sizeY << "\n255\n";

	for(int j = sizeY - 1; j >= 0; j--)
	{
		for(int i = 0; i < sizeX; i++)
		{
			size_t pixelIndex = j * sizeX + i;
			// Scale color to 0-255 range0
			int color = fb[pixelIndex] * 255;
			// write color to output file
			ofs << color << " " << color << " " << color << "\n";
		}
	}

	// close .ppm file
	ofs.close();
}

void Image::ParseJson(const char* fileName)
{
	std::cout << "Parsing JSON data..." << std::endl;

	// Get path to file
	std::string filePath = "test_cases/" + std::string(fileName) + ".json";

	// Open JSON data file
	std::ifstream jsonData(filePath);
 	if(!jsonData.is_open())
	{
		std::cout << "JSON data file not opened correctly" << std::endl;
		return;
	}

	// Stack containing open brackets
	std::stack<char> bracketStack;

	// Read JSON data line by line
	std::string line;
	while(!jsonData.eof())
	{
		// Read line
		std::getline(jsonData, line);
		std::istringstream iss(line.c_str());

		// Read first character in line
		char c;
		iss >> c;

		// If character is opening bracket add it to the stack
		if(c == '{' || c == '[')
		{
			bracketStack.push(c);
			continue;
		}

		// If character is closing brackt check if it matches
		// last opening bracket
		if(c == '}' || c == ']')
		{
			// If character matches opening bracket remove last bracket from the stack
			if(bracketStack.top() == '{' && c == '}' ||
				bracketStack.top() == '[' && c == ']')
			{
				bracketStack.pop();
				continue;
			}

			// If brackets do not match stop parsing JSON file
			std::cout << "Opening and closing brackets do not match" << std::endl;
			return;
		}

		// Read next character in line
		iss >> c;

		// String used for flushing
		std::string s;

		switch(c)
		{
		case 'r':
			// Do nothing new region will be added on 'p
			
			// Check if opening brackt is on the same line
			iss >> s >> c;
			if(c == '[') bracketStack.push(c);
			break;
		case 'p':
			// Add new region to the vector
			regions.push_back(Region());

			// Check if opening brackt is on the same line
			iss >> s >> c;
			if(c == '[') bracketStack.push(c);
			break;
		case 'x':
			// Create new position
			regions.back().positions.push_back(Position());

			// Add x value to the last position

			// Flush rest of string data
			iss >> s ;
			// Read number from line
			iss >> regions.back().positions.back().x;
			break;
		case 'y':
			// Add y value to the last position
			// Flush rest of string data
			iss >> s;
			// Read number from line
			iss >> regions.back().positions.back().y;
			break;
		case 's':
			// Initialize frame buffer
			// flush data before first number
			iss >> s >> s;
			// Read x and y
			iss >> sizeX >> s >> sizeY;
			fb = new int[sizeX * sizeY]();
			break;
		default:
			break;
		}

	}

	// Close JSON data file
	jsonData.close();

}

void Image::CalculatePixelColors()
{
	std::cout << "Calculating pixel color values..." << std::endl;

	// Loop over every region
	for(auto& region : regions)
	{
		// Find Axis Aligned Bounding Box for current region
		// Only test pixels inside AABB
		Position min(1, 1);
		Position max(0, 0);
		for(auto& position : region.positions)
		{
			// Find minimum x and y values
			if(position.x < min.x) min.x = position.x;
			if(position.y < min.y) min.y = position.y;

			// Find maximum x and y values
			if(position.x > max.x) max.x = position.x;
			if(position.y > max.x) max.y = position.y;
		}

		// Conver min and max values to pixel index
		PositionToPixelIndex(min);
		PositionToPixelIndex(max);

		// Loop over every pixel in AABB for current region
		for(int i = min.x; i <= max.x; i++)
		{
			for(int j = min.y; j <= max.y; j++)
			{
				// Calculate pixel index
				int pixelIndex = j * sizeX + i;
				
				// Calculate pixel position
				Position pixelPosition = PixelIndexToPosition(i, j);

				//std::cout << "i: " << i << ", j: " << j << std::endl;
				//std::cout << "p.x: " << pixelPosition.x << ", p.y: " << pixelPosition.y << std::endl;

				if(PixelIsInRegion(region.positions, pixelPosition))
					fb[pixelIndex] = 1;
			}
		}
	}

}

void Image::PositionToPixelIndex(Position& position)
{
	// Get pixel index from its position
	position.x = std::round(position.x * (sizeX - 1));
	position.y = std::round(position.y * (sizeY - 1));
}

Position Image::PixelIndexToPosition(int i, int j)
{
	// Get pixel position in 0-1 range
	// Add 0.5f to i and j to get center of pixel
	return Position((i + 0.5001f) / sizeX, (j + 0.5f) / sizeY);
}

bool Image::PixelIsInRegion(const std::vector<Position>& region, const Position& pixelPosition)
{
	// Create all triangles from one point
	// Check the number of triangles the point is int
	// odd = inside, even = outside

	// Number of triangles pixel is inside
	int numberOfTriangles = 0;

	for(int i = 1; i < region.size() - 1; i++)
	{
		if(PixelIsInTriangle(region[0], region[i], region[i + 1], pixelPosition))
			numberOfTriangles++;
	}

	return numberOfTriangles % 2;
}
bool Image::PixelIsInTriangle(const Position& A, const Position& B, const Position& C, const Position& P)
{
	// Points A, B, C are points of the triangle, point P is PixelPosition
	// 
	// P is inside ABC if and only if vectors ABxAP, BCxBP and CAxCP
	// point in the same direction relative to triangle plane 
	// this means z component of all 3 cross products must be all positive or all negative
	
	// Calculate z component of cross products
	float z1, z2, z3;
	z1 = CrossProductZComponent(A, B, P);
	z2 = CrossProductZComponent(B, C, P);
	z3 = CrossProductZComponent(C, A, P);

	// Check if all 3 components are >0
	if(z1 > 0) return z2 > 0 && z3 > 0;

	// Check if all 3 components are <0
	return z2 < 0 && z3 < 0;
}

float Image::CrossProductZComponent(const Position& A, const Position& B, const Position& P)
{
	// Z component of Cross product of vectors AB and AP
	// (v1 x v2).z = x1y2 - x2y1

	// AB.x = A.x - B.x   AB.y = A.y - B.y
	// AP.x = A.x - P.x   AP.y = A.y - P.y
	// (AB x AP).z = AB.x * AP.y - AP.x * AB.y
	return  (A.x - B.x) * (A.y - P.y) - (A.x - P.x) * (A.y - B.y);
}
