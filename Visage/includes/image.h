#pragma once

#include <vector>

// Position Declaration 
struct Position
{
	// Position default constructor
	Position() : x(0), y(0) {}

	Position(float x, float y) : x(x), y(y) {}

	// Coordinates of this position
	float x, y;
};

// Region Declaration 
struct Region
{
	// Region default constructor
	Region()
	{
		positions = std::vector<Position>();
	}

	// List of all positions in this region
	std::vector<Position> positions;
};

// Image Declaration 
class Image
{
public:
	// Image public methods

	/**
	 * Image constructor
	 *
	 * @param fileName string containing name of JSON data file
	 */
	Image(const char* fileName);

	// Image destructor
	~Image() 
	{
		// Free FrameBuffer memory
		if(fb) delete[] fb;
	}

	/**
	 * Write image in ppm format
	 */
	void DrawImage(const char* fileName);

private:
	// Image private methods

	/**
	 * Parse Json function
	 *
	 * @param fileName string containing path to JSON data file
	 */
	void ParseJson(const char* fileName);

	/*
	 * Calculate color for every pixel and fill FrameBuffer with 0 (black) and 1 (white) values
	 */
	void CalculatePixelColors();

	/**
	 * Transfer position from 0-1 range to pixel index
	 *
	 * @parm[out] position position in 0-1 range to pixel index
	 */
	void PositionToPixelIndex(Position& position);

	/**
	 * Transfer pixel x and y position to 0-1 range
	 *
	 * @parm i, j position of pixel on x and y axis
	 * @returns Position of pixer in 0-1 range
	 */
	Position PixelIndexToPosition(int i, int j);

	/**
	 * Check if pixel is inside given region
	 *
	 * @parm region vector of positions defining one region
	 * @param pixelPosition pixel position in 0-1 range
	 * @returns true if pixel is inside region
	 */
	bool PixelIsInRegion(const std::vector<Position>& region, const Position& pixelPosition);

	/**
	 * Check if pixel is inside triangle defined by 3 points
	 *
	 * @parm A, B, C position of triangle points
	 * @param P pixel position in 0-1 range
	 * @returns true if pixel is inside triangle
	 */
	bool PixelIsInTriangle(const Position& A, const Position& B, const Position& C, const Position& P);

	/**
	 * Calculate cross product of vectors AB and AP
	 * Vectors are 2D so frist two components are 0 (z1 = z2 = 0)
	 * 3rd component is one we are looking and it is equal to x1y2 - x2y1
	 *
	 * @parm A, B positions of triange vertices
	 * @param P position of pixel
	 * @returns third component of cross product
	 */
	float CrossProductZComponent(const Position& A, const Position& B, const Position& P);

private:
	// Image private data

	// List of all regions
	std::vector<Region> regions;

	// FrameBuffer containing all pixel color data
	int* fb = nullptr;
	// Size of image
	int sizeX, sizeY;
};